export interface DataTablesColumns extends Object {
    name: string,
    html?: string,
}