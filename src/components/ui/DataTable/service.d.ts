export interface DataTable {
  pageLimit: number,
  page: number,
  filter: string,
  column: DataTablesColumns | null,
}

export interface DataTableInterface {
  columns: DataTablesColumns[],
  data: null | string,
}

export interface DataTablesColumns {
  name: string,
  html?: string,
  type: string,
  sortable: boolean,
  expandable?: boolean,
  order?: string,
}