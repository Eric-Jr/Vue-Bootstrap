interface XHROpts {
  method: string,
  headers?: Headers & {
    [key: string]: any,
  },
  credentials?: boolean,
  body?: string & object,
  timeout?: number,

  [key: string]: any,
}