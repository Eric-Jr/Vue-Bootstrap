const xhr = (url: string, opts?: XHROpts): Promise<any> => {
  return new Promise((resolve, reject) => {
    const request = new XMLHttpRequest();
    const params: XHROpts = {
      method: 'GET',
      timeout: 30000,
    };

    Object.keys(params).forEach(key => {
      if (opts && opts[key]) params[key] = opts[key];
    });

    request.withCredentials = params.credentials ? params.credentials : false;
    request.timeout = params.timeout ? params.timeout : 30000;

    // Validate URL
    if (url.length > 0) request.open(params.method, url, true);
    else console.error('Invalid URL');

    if (params.headers) {
      if (params.headers.entries) {
        for (const header of params.headers) request.setRequestHeader(header[0], header[1]);
      } else {
        Object.keys(params.headers).forEach(key => {
          if (params.headers) request.setRequestHeader(key, params.headers[key]);
        });
      }
    }

    request.onload = function () {
      if (this.status >= 200 && this.status < 400)
        resolve({
          response: this.response,
          request: this,
        });
      else reject({
        response: this.response,
        request: this,
      });
    };

    request.onerror = function () {
      reject({
        response: this.response,
        request: this,
      });
    };

    request.ontimeout = function () {
      reject({
        response: `Request timed out after ${(params.timeout ? params.timeout : 30000) / 1000} seconds`,
        request: this,
      });
    };

    request.onloadend = () => {
      resolve();
    };
    request.send((params.body) ? params.body : '');
  });
};

export default xhr;